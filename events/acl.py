import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_weather_data(city, state):
    # get lat and lon
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    resp = requests.get(url, params=params)
    content = resp.json()
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    # get current weather
    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    resp = requests.get(url, params=params)
    json_resp = resp.json()

    try:
        temp = json_resp["main"]["temp"]
        description = json_resp["weather"][0]["description"]
    except (KeyError, IndexError):
        return None

    return {"temp": temp, "description": description}



def get_location_photo(city, state):
    headers = {
        'Authorization': PEXELS_API_KEY
    }
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    resp = requests.get(url, headers=headers)
    print(resp)
    return resp.json()["photos"][0]["url"]